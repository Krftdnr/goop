#include "GLFW/glfw3.h"
#include "glad/glad.h"
#include "Logging.h"
#include <iostream>

int main() {

	// Init GLFW
	if (glfwInit() == GLFW_FALSE) {
		std::cout << "Failed to initialize GLFW" << std::endl;
		throw std::runtime_error("Failed to initialize GLFW");
	};

	GLFWwindow* myWindow = glfwCreateWindow(600, 600, "Title", nullptr, nullptr);

	//glfwSetWindowUserPointer(myWindow, this);
	//glfwSetWindowSizeCallback(myWindow, GlfwWindowResizedCallback);
	glfwMakeContextCurrent(myWindow);
	
	// Init GLAD
	if (gladLoadGLLoader((GLADloadproc)glfwGetProcAddress) == 0) {
		std::cout << "Failed to initialize GLAD" << std::endl;
		throw std::runtime_error("Failed to initialize GLAD");
	}

	// Compile shaders
	const char* vertexShader =
		"#version 400\n"
		"in vec2 position;"
		"in vec3 colour;"
		"out vec3 f_colour;"
		"void main() {"
		"  gl_Position = vec4(position, 0.0, 1.0);"
		"  f_colour = colour;"
		"}";
	GLuint vs = glCreateShader(GL_VERTEX_SHADER);
	glShaderSource(vs, 1, &vertexShader, NULL);
	glCompileShader(vs);

	const char* fragmentShader =
		"#version 400\n"
		"in vec3 f_colour;"
		"out vec4 outColour;"
		"void main() {"
		"  outColour = vec4(f_colour, 1.0);"
		"}";
	GLuint fs = glCreateShader(GL_FRAGMENT_SHADER);
	glShaderSource(fs, 1, &fragmentShader, NULL);
	glCompileShader(fs);

	GLuint shaderProgram = glCreateProgram();
	glAttachShader(shaderProgram, fs);
	glAttachShader(shaderProgram, vs);
	glLinkProgram(shaderProgram);

	GLint status;
	glGetShaderiv(shaderProgram, GL_COMPILE_STATUS, &status);
	if (status == GL_FALSE) {
		std::cout << "failure" << std::endl;
	}

	// Create VBO / VAO
	float points[] = {
		0.0f, 0.5f, 1.0f, 0.0f, 0.0f,
		-0.5f, -0.5f, 0.0f, 1.0f, 0.0f,
		0.5f, -0.5f, 0.0f, 0.0f, 1.0f,
	};
	GLuint vbo = 0;
	glGenBuffers(1, &vbo);
	glBindBuffer(GL_ARRAY_BUFFER, vbo);
	glBufferData(GL_ARRAY_BUFFER, sizeof(points), points, GL_STATIC_DRAW);
	GLuint vao = 0;
	glGenVertexArrays(1, &vao);
	glBindVertexArray(vao);
	glEnableVertexAttribArray(0);
	glBindBuffer(GL_ARRAY_BUFFER, vbo);
	GLint posAttrib = glGetAttribLocation(shaderProgram, "position");
	glEnableVertexAttribArray(posAttrib);
	glVertexAttribPointer(posAttrib, 2, GL_FLOAT, GL_FALSE,
		5 * sizeof(float), 0);
	GLint colAttrib = glGetAttribLocation(shaderProgram, "colour");
	glEnableVertexAttribArray(colAttrib);
	glVertexAttribPointer(colAttrib, 3, GL_FLOAT, GL_FALSE,
		5 * sizeof(float), (void*)(2 * sizeof(float)));
	glEnableVertexAttribArray(posAttrib);
	// while true
	while(!glfwWindowShouldClose(myWindow)) {
	//  Draw stuff
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		glUseProgram(shaderProgram);
		glBindVertexArray(vao);
		glDrawArrays(GL_TRIANGLES, 0, 3);

	//  poll GLFW events
		glfwPollEvents();
	//  swap buffers
		glfwSwapBuffers(myWindow);
	}

	// delete everything
	glDeleteProgram(shaderProgram);
	glfwTerminate();

	return 0;
}