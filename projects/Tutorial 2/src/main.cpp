#include "florp/app/Application.h"
//#include "florp/game/BehaviourLayer.h"
#include "florp/game/ImGuiLayer.h"
#include "SceneBuilder.h"
#include "RenderLayer.h"

int main() {
	// Create our application
	florp::app::Application* app = new florp::app::Application;

	// Set up our layers
	//app->AddLayer<florp::game::BehaviourLayer>();
	app->AddLayer<SceneBuilder>();
	app->AddLayer<RenderLayer>();
	app->AddLayer<florp::game::ImGuiLayer>();
	
	app->Run();

	return 0;
} 
