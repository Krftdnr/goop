#pragma once
#include <glm/glm.hpp>
#include "FrameBuffer.h"

struct CameraComponent {
    // True if this camera is the main camera for the scene (ie, what gets rendered fullscreen)
    bool IsMainCamera;
    // The render target that this camera will render to
    FrameBuffer::Sptr Buffer;
    // The Color to clear to when using this camera
    glm::vec4 ClearCol = glm::vec4(0, 0, 0, 1);

    // The projection matrix for this camera
    glm::mat4 Projection;
};

